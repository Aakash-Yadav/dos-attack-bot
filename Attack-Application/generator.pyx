
from cpython cimport array
from random import choice 
from string import ascii_letters


cdef int x
cdef array.array Array_of_Random  = array.array('i',[x for x in range(0,9)])

cdef Random_Phone_Number():
    cdef int x 
    return '%s%s'%(9,''.join(str(choice(Array_of_Random)) for x in range(9)))

cdef Random_Email(str name, int char_num):
    cdef int v
    return '%s%s%s'%(name,''.join(choice(ascii_letters) for v in range(char_num)),"@gmail.com")

cdef Random_Name(str name):
    cdef int v 
    return  ('%s %s'%(name,''.join(choice(ascii_letters) for v in range(5)))).lower()

cpdef random_name(str name):
    return Random_Name(name)


cdef array.array inst_value = array.array('i',[1,4,7,14,12,8,9,10,11,13,15,-1])

cpdef instute():
    return '%s'%(choice(inst_value))

cdef random_password(str name):
    cdef int val = 8 
    cdef str new_name = Random_Name(name).replace(' ','')
    return '%s%s'%(new_name, Random_Phone_Number()[1:4])

cpdef random_password_gen(str name):
    return random_password(name)

cpdef return_email(str name):
    return Random_Email(name,5).lower()

cpdef return_number():
    return Random_Phone_Number()



