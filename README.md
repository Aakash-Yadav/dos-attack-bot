
## Distributed Denial of Service
![alt text](https://xp.io/storage/1GW9XxXC.png)
### .

## What is a DDoS attack?

A distributed denial-of-service (DDoS) attack is a malicious attempt to disrupt the normal traffic of a targeted server, service or network by overwhelming the target or its surrounding infrastructure with a flood of Internet traffic.

DDoS attacks achieve effectiveness by utilizing multiple compromised computer systems as sources of attack traffic. Exploited machines can include computers and other networked resources such as IoT devices.

From a high level, a DDoS attack is like an unexpected traffic jam clogging up the highway, preventing regular traffic from arriving at its destination.

![alt text](https://xp.io/storage/1GVY3PYw.png)

## How does a DDoS attack work?
DDoS attacks are carried out with networks of Internet-connected machines.

These networks consist of computers and other devices (such as IoT devices)which have been infected with malware, allowing them to be controlled remotely by an attacker. These individual devices are referred to as bots (or zombies), and a group of bots is called a botnet.

Once a botnet has been established, the attacker is able to direct an attack by sending remote instructions to each bot.

When a victim’s server or network is targeted by the botnet, each bot sends requests to the target’s IP address, potentially causing the server or network to become overwhelmed, resulting in a denial-of-service to normal traffic.

Because each bot is a legitimate Internet device, separating the attack traffic from normal traffic can be difficult.

![alt text](https://xp.io/storage/1GWmnfPe.png)

# conclusion!
 It is illegal and prohibited by the Government, Performing a DDosing attack is a cybercrime.

It is completely illegal as defined by the Computer Fraud and Abuse Act and cybercriminals may face imprisonment charges of a five million dollars ($5,00,000) Fine and ten years of jail.

DDosing, also known as Distributed denial of service attack, is a form of cyber attack where a server (the victim) is flooded with internet traffic and packets of spam data from various sources which might be computers or other servers, to make the victim server inaccessible.

![alt text](https://xp.io/storage/1GYEUHf4.png)

### Is DDosing illegal? Explained
Yes, it is a cyber crime in many countries. It is illegal as defined by the Computer Fraud and Abuse Act and the cyber criminals may face imprisonment charges.

The countries that signed the United Nations Convention against Transnational Organized Crime, doesn’t matter if they have a law against it or not they will have to comply and treat it as a crime if the attack originated from their country.

In European countries, the suspect will at least be arrested no matter if they performed an SYN flood or some other method of dos, they will face legal charges.

In January of 2013, a petition was posted on the whitehouse gov website asking to recognize it as a form of protest because of its similarity occupy movement. The unaware servers that get exploited will during a denial of service attack also sue the attacker.

Adam Brown (Computer Security Expert) says, You can receive a prison sentence, a fine, or both, if you execute a DDoS attack or perform, provide or receive stress or booter services. A distributed service denial is referred to by DDOS.

In such an attack using this technique on a website, the computer user inundates a target Website with numerous service requests and overloads the server to slow down or discontinue requests or services, making the Website and its business inoperative.

Experienced users target a Web site with thousands of “zombie” botnets that could be corrupted with malicious software with bombardment. Cyber criminals take advantage of the attack to extort and compel corporations to pay for stopping the attack. There are websites that use these tactics to cripple opposing sites.

Whilst some observers defend DDOS targeting a website, it is illegal under the Federal Computer Fraud and Abuse Act as a mass protest against an insulting website. Crimes of up to 10 years and up to $500,000 are subject to sentences. Conspiracy is prosecuted for up to five years and $250,000 in penalty.

### thank you

